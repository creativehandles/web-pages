# Web Pages Package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/web-pages.svg?style=flat-square)](https://packagist.org/packages/creativehandles/web-pages)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/web-pages.svg?style=flat-square)](https://packagist.org/packages/creativehandles/web-pages)
![GitHub Actions](https://github.com/creativehandles/web-pages/actions/workflows/main.yml/badge.svg)


## Installation

You can install the package via composer:

```bash
composer require creativehandles/web-pages
```

## Usage
Publish required files (migrations,routes,views etc) to CORE CMS

```php
php artisan vendor:publish --provider="Creativehandles\WebPages\WebPagesServiceProvider"
```

Add following environment variable. This is a global variable for all packages.Recommends to have it in CORE-CMS. DO NOT CHANGE

```php
MEDIABLE_DISK=mediable
```

Migrate database tables
```php
php artisan migrate
```

Config file
```php
    "api" => [
        'route_prefix' => 'api/v1/',
        'middleware' => ['api']
    ],
    "web" => [
        'route_prefix' => 'plugins/',
        'middleware' => ['web']
    ],

```
### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

-   [Deemantha](https://github.com/creativehandles)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
