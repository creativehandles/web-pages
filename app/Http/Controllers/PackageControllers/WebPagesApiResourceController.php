<?php

namespace App\Http\Controllers\PackageControllers;

use Creativehandles\WebPages\Http\Controllers\ApiControllers\WebPageApiController;
use Illuminate\Http\Request;

/**
 * @group Web Pages
 *
 * APIs for Web pages
 */
class WebPagesApiResourceController extends WebPageApiController
{
    /**
     * Get all web pages
     *
     * @queryParam search string Query to search. you can search through title or description. Example: Lorem
     * @queryParam order[0][column] string  Order column  Possible values("id","title","date","created_at","updated_at")
     * @queryParam order[0][dir] string  Possible values(asc,desc)
     *
     *
     * @queryParam where[0][column]
     * If u are using multiple where query params, it will support to AND operator in mysql
     * Possible values (id,title,short_description,status,author_id,date)
     *
     *
     * Possible where types
     *  ["table.column","relationship:column_checking"].
     *
     *
     * NOTE: above items with ":" are for relationships. relationship:column_checking
     * ex categories:title => you are looking for category name in categories relationship
     * Possible Values (categories:title,tags:name).
     *
     *
     *
     * @queryParam where[0][operator] Possible operators (=,>,<,>=,<=,like) if u are using like make sure to add % properly in the value as it is mysql operators. Example: like
     *
     * @queryParam where[0][value] Value you are looking for. Example: %Lorem%
     *
     *
     * @queryParam with[0]  Load relationships.
     * @queryParam with[1]  Load relationships.
     *
     * possible values (tags,categories,images)
     *
     * @queryParam withoutRelations boolean drop all relations.
     *
     * Possible values (true)
     *
     *
     * Ex link /api/v1/plugins/web-pages?order[0][column]=id&order[0][dir]=desc&with[0]=tags&where[0][column]=author_id&where[0][operator]==&where[0][value]=2
     *
     *
     * @responseFile responses/web-pages-all.200.json
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * Get single web page by id
     *
     *
     * @queryParam with[0]  Load relationships . Example: categories
     * @queryParam with[1]  Load relationships. Example: tags
     * @queryParam with[2]  Load relationships
     * possible values (tags,categories,images). Example: images
     *
     *
     * @responseFile responses/web-pages-single.200.json
     */
    public function show(Request $request, $id)
    {
        return parent::show($request, $id);
    }
}
