<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    "api" => [
        'route_prefix' => 'api/v1/',
        'middleware' => ['api']
    ],
    "web" => [
        'route_prefix' => 'plugins/',
        'middleware' => ['web']
    ],

    "resourceName" => "web-pages",

];
