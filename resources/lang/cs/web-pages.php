<?php

return [
    "Pages"=>"Stránky",
    "pages"=>"Stránky",
    "Update" =>"Aktualizovat stránku",
    "Page content" =>"Obsah stránky",
    "Create new" =>"Vytvořit novou",
    "title" =>"Titulek",
    "short_description" =>"Stručný popis",
    "main_description" =>"Hlavní popis",
    "additional_description" =>"Další popis",
    "slug" =>"Identifikátor",
    "date" =>"Datum",
    "status" =>"Viditelnost",
    "Draft" =>"Návrh",
    "author_id" =>"Autor",
    "Please select" =>"Prosím vyberte",
    "image_url"=>"Hlavní obrázek",
    "published_date"=>"Datum zveřejnění",
    "author"=>"Autor",
    "N/A"=>"Bez autora",
    "Draft" => "Koncept",
    "Pending" => "Skryto",
    "Published" => "Publikováno",
    ""=>"-"
];
