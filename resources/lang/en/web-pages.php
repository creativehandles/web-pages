<?php

return [
    "Pages"=>"Pages",
    "pages"=>"Pages",
    "Update" => "Update Page",
    "Page content" => "Page Content",
    "Create new" => "Create New",
    "title" => "Title",
    "short_description" => "Short Description",
    "main_description" => "Main Description",
    "additional_description" => "Additional Description",
    "slug" => "Slug",
    "date" => "Date",
    "status" => "Status",
    "Draft" => "Draft",
    "author_id" => "Author",
    "Please select" => "Please select",
    "image_url"=>"Main Image",
    "published_date"=>"Published Date",
    "author"=>"Author",
    "N/A"=>"N/A",
    "Draft" => "Draft",
    "Pending" => "Pending",
    "Published" => "Published",
    ""=>"-"
];
