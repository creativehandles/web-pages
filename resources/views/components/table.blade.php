<div class="row">
    <div class="col-md-12 col-12 mb-2">
        <div class="mb-2 pull-left">
            @component("Admin.components.language-picker")
            @endcomponent
        </div>
        <div class="mb-2 pull-right">
            <a href="{{route('admin.web-pages.create')}}" class="btn btn-secondary btn-block-sm"><i class="ft-file-plus"></i> {{__('web-pages.Create new')}}</a>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped full-width" id="thegrid">
        <thead>
        <tr>
            <th>{{__('web-pages.id')}}</th>
            <th>{{__('web-pages.title')}}</th>
            <th>{{__('web-pages.slug')}}</th>
            <th>{{__('web-pages.published_date')}}</th>
            <th>{{__('web-pages.status')}}</th>
            <th>{{__('web-pages.author')}}</th>
            <th class="action-col">{{__('general.Translate')}}</th>
            <th class="action-col" >{{__('general.Update')}}</th>
            <th class="action-col" >{{__('general.Delete')}}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
