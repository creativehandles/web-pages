<?php

use Illuminate\Support\Facades\Route;

Route::group(['name' => 'web-pages-api', 'groupName' => 'web-pages-api','prefix'=>'plugins'], function () {
    Route::resource('web-pages', 'PackageControllers\WebPagesApiResourceController')->only(['index','show']);
    // Route::get('/web-pages', 'PackageControllers\WebPagesApiResourceController@index')->name('web-pages-api.index');
    // Route::get('/web-pages/{id}', 'PackageControllers\WebPagesApiResourceController@show')->name('web-pages-api.show');
});

