<?php

/** posts BreadCrumbs */
// Dashboard > posts
Breadcrumbs::for('admin.web-pages.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('web-pages.Pages'), route('admin.web-pages.index'));
});

// Dashboard > posts > create
Breadcrumbs::for('admin.web-pages.create', function ($trail) {
    $trail->parent('admin.web-pages.index');
    $trail->push(__('web-pages.Create new'), route('admin.web-pages.create'));
});

// Dashboard > posts > edit
Breadcrumbs::for('admin.web-pages.edit', function ($trail, $post) {
    $trail->parent('admin.web-pages.index');
    $trail->push(__('web-pages.Update'), route('admin.web-pages.edit', $post->post ?? ''));
});
