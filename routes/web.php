<?php

use Illuminate\Support\Facades\Route;


Route::group(['name' => 'Web Pages', 'groupName' => 'Web Pages'], function () {
    Route::get('web-pages/grid', 'PackageControllers\WebPagesCrudController@grid')->name('web-pages.grid');
    Route::resource('web-pages', 'PackageControllers\WebPagesCrudController');
});


