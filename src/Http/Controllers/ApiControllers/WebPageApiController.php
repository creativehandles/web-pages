<?php

namespace Creativehandles\WebPages\Http\Controllers\ApiControllers;

use App\Http\Controllers\BaseControllers\BaseApiController;
use Creativehandles\WebPages\Repositories\WebPagesRepository;
use Exception;
use Illuminate\Http\Request;

class WebPageApiController extends BaseApiController
{


    public function setResourceCollection(){
        
    }

    public function getSearchableColumns()
    {
        return [
            "title",
            "short_description",
            "main_description",
            "additional_description"
        ];
    }

    public function getDefaultRelations()
    {
        return ["categories","tags"];
    }

    public function getOrderableColumns()
    {
        return [
            "ch_web_pages.id" => "id",
            "ch_web_pages.title" => "title",
            "ch_web_pages.date" => "date",
            "ch_web_pages.created_at" => "created_at",
            "ch_web_pages.updated_at" => "updated_at"
        ];
    }


    public function getService(){

    }

    public function getRepository(){
        return app(WebPagesRepository::class);
    }

}
