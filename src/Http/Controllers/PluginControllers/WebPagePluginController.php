<?php

namespace Creativehandles\WebPages\Http\Controllers\PluginControllers;

use App\Http\Controllers\BaseControllers\BasePluginController;
use App\Traits\ManageCategoriesTrait;
use App\Traits\ManageTagsTrait;
use App\Traits\MediaManagerTrait;
use Creativehandles\WebPages\Repositories\WebPagesRepository;
use Creativehandles\WebPages\Services\WebPagesPluginService;
use Creativehandles\WebPages\WebPages;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class WebPagePluginController extends BasePluginController
{
    use MediaManagerTrait,ManageCategoriesTrait,ManageTagsTrait;


    public function getService()
    {
        return app(WebPagesPluginService::class);
    }

    public function getRepository()
    {
        return app(WebPagesRepository::class);
    }

    public function getViewsFolder($override = null)
    {
        return "Admin.WebPages.";

        // return "blog-posts::";
    }

    public function getIndexRoute()
    {
        return WebPages::getIndexRoute();
    }

    public function getCreateRoute()
    {
        return WebPages::getCreateRoute();
    }

    public function getEditRoute()
    {
        return WebPages::getEditRoute();
    }

    public function getDeleteRoute()
    {
        return WebPages::getDeleteRoute();
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/posts', 'PostsController')->name('posts');
     */
    public function getResourceName()
    {
        return "web-pages";
    }


    public function validateRequest(Request $request)
    {
        $request->validate([
            // 'slug' => 'required|unique_translation:ch_posts,slug,' . $request->id,
            'title' => "required",
            'main_description' => "required",
            'status' => "required",
            'seo_url' => "required",
        ]);
    }

    /**
     * Show the form for creating a new Items.
     *
     * @return Response
     */
    public function create()
    {
        $statuses = WebPages::getStatuses();
        $authors = $this->service->getAuthors();
        return view($this->viewsFolder . 'add', ['statuses' => $statuses])->with('authors', $authors);
    }


    /**
     * Show the form for editing the specified Items.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        try {
            $items = $this->repository->byId($id);

            $statuses = WebPages::getStatuses();
            $authors = $this->service->getAuthors();
        } catch (ModelNotFoundException $e) {
            \Session::flash('error', __('Item not found'));
            return redirect(route($this->indexRoute));
        } catch (Exception $e) {
            \Session::flash('error', __('Error Occured'));
            return redirect(route($this->indexRoute));
        }


        return view($this->viewsFolder . 'add')->with('model', $items)
            ->with('authors', $authors)
            ->with('statuses', $statuses);
    }


    /**
     * Update the specified Items in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validateRequest($request);

        try {
            $formatData = $request->except(['multi_attachments', 'multiImageList','categories','tags']);

            if ($id) {
                $formatData['id'] = $id;
            }

            $items = $this->repository->updateOrCreate($formatData, 'id');

            $categories = $request->get('categories',[]);
            $tags = $request->get('tags',[]);

            //sync categories
            $this->updateCategories($items,$categories);
            //sync tags
            $this->updateTags($items,$tags);

            //process main image for post
            // if ($request->has('image_url')) {
            //     $media = $this->processOne($request, 'image_url');
            //     $items->syncMedia($media, ['image_url-'. app()->getLocale()]);
            // }

            $imageList = $request->get('multiImageList', []); //here we are getting an array of images

            if ($imageList) {
                //link images
                if (count($imageList) > 0) {
                    $this->processImages($imageList, $items);
                } else {
                    $items->images()->where('model', $this->model)->delete();
                }
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => [__('Error Occured')], 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => __('Item updated successfully.'), 'data' => $items], 200);
        } else {
            \Session::flash("success", __('Item updated successfully.'));
            return redirect(route($this->indexRoute));
        }
    }








    public function grid(Request $request)
    {
        try {

            $searchColumns = ['ch_web_pages.id', 'ch_web_pages.title'];
            $orderColumns = ['1' => 'ch_web_pages.id'];
            $with = [];
            $data = $this->repository->getDataforDataTables($searchColumns, $orderColumns, '1', $with);
            $translateBtns = "";
            $forgetBtns = "";

            $data['data'] = $data['data']->map(function ($item) use ($translateBtns, $forgetBtns) {
                $return = [
                    $item->id,
                    $item->title,
                    $item->slug,
                    $item->date,
                    __('web-pages.'.$item->status),
                     $item->author? $item->author->name : __('web-pages.N/A'),
                    $this->translateButton($item, WebPages::getEditRoute(), ['web_page' => $item->id]),
                    $this->editButton($item, WebPages::getEditRoute(), ['web_page' => $item->id]),
                    $this->deleteButton($item, WebPages::getDestroyRoute()),
                ];

                return $return;
            });

            return json_encode($data);
        } catch (Exception $e) {
            return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage() . $e->getTraceAsString()], 500);
        }
    }
}
