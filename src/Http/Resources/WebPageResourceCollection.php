<?php

namespace Creativehandles\WebPages\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\BaseResourceCollection;

class WebPageResourceCollection extends BaseResourceCollection
{

    public function getResourceClass(): string
     {
       return  WebPageResource::class;
     }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
