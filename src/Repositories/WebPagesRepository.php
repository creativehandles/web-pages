<?php

namespace Creativehandles\WebPages\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\WebPages\Http\Resources\WebPageResource;
use Creativehandles\WebPages\Http\Resources\WebPageResourceCollection;
use Creativehandles\WebPages\Models\WebPage;

class WebPagesRepository extends BaseEloquentRepository{


    public function getModel()
    {
        return new WebPage();
    }

    public function getResource()
    {
        return WebPageResource::class;
    }

    public function getResourceCollection()
    {
        return WebPageResourceCollection::class;
    }

    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null){
        return parent::getDataforDataTables($searchColumns,$orderColumns,$defOrderColumnKey,$with,$where);
    }


    public function getDataIndexforDataTables($collection)
    {
        return $collection;
    }
}
