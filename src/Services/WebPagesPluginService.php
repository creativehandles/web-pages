<?php

namespace Creativehandles\WebPages\Services;

use App\Repositories\CoreRepositories\UserRepository;

class WebPagesPluginService{


    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAuthors(){
        return $this->userRepository->getAuthors();
    }

}
