<?php

namespace Creativehandles\WebPages;

class WebPages
{
    // Build your next great package.

    public static function getIndexRoute()
    {
        return config('web-pages.IndexRoute', "admin.web-pages.index");
    }

    public static function getCreateRoute()
    {
        return config('web-pages.CreateRoute', "admin.web-pages.create");
    }

    public static function getStoreRoute()
    {
        return config('web-pages.StoreRoute', "admin.web-pages.store");
    }

    public static function getEditRoute()
    {
        return config('web-pages.EditRoute', "admin.web-pages.edit");
    }

    public static function getUpdateRoute()
    {
        return config('web-pages.UpdateRoute', "admin.web-pages.update");
    }

    public static function getDeleteRoute()
    {
        return config('web-pages.DeleteRoute', "admin.web-pages.delete");
    }

    public static function getDestroyRoute()
    {
        return config('web-pages.DestroyRoute', "admin.web-pages.destroy");
    }

    public static function getGridRoute()
    {
        return config('web-pages.GridRoute', "admin.web-pages.grid");
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/posts', 'PostsController')->name('posts');
     */
    public static function getResourceName()
    {
        return config('web-pages.resourceName', "web-pages");
    }

    public static function getStatuses()
    {
        return [
            "Draft" => __('post.Draft'),
            "Pending" => __('post.Pending'),
            "Published" => __('post.Published')
        ];
    }
}
