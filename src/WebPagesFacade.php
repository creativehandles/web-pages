<?php

namespace Creativehandles\WebPages;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\WebPages\Skeleton\SkeletonClass
 */
class WebPagesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'web-pages';
    }
}
