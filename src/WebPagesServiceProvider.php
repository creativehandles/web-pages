<?php

namespace Creativehandles\WebPages;

use Illuminate\Support\ServiceProvider;

class WebPagesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'web-pages');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'web-pages');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('web-pages.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../database/migrations/create_ch_pages_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', now()->addSeconds(1)->timestamp) . '_create_ch_pages_table.php'),
                __DIR__ . '/../database/migrations/alter_ch_pages_table_for_translation_support.php.stub' => database_path('migrations/' . date('Y_m_d_His', now()->addSeconds(2)->timestamp) . '_alter_ch_pages_table_for_translation_support.php'),
                // you can add any number of migrations here
            ], 'migrations');

            // Publishing the views.
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/Admin/WebPages/'),
            ], 'views');

            // Publishing  controller.
            $this->publishes([
                __DIR__.'/../app/Http/Controllers/' => app_path('/Http/Controllers/'),
            ], 'pluginController');

            // Publishing the translation files.
            $this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/'),
            ], 'lang');

             //publishing routes and breadcrumbs
             $this->publishes([
                __DIR__.'/../routes/web.php' => base_path('routes/packages/package-routes/web-pages-web-routes.php'),
                __DIR__.'/../routes/api.php' => base_path('routes/packages/api-routes/web-pages-api-routes.php'),
                __DIR__.'/../routes/breadcrumbs' => base_path('routes/packages/breadcrumbs/'),
            ], 'routes');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'web-pages');

        // Register the main class to use with the facade
        $this->app->singleton('web-pages', function () {
            return new WebPages;
        });
    }


    protected function registerRoutes()
    {

        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });

        Route::group($this->apiRouteConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => config('web-pages.web.route_prefix'),
            'middleware' => config('web-pages.web.middleware'),
        ];
    }


    protected function apiRouteConfiguration()
    {
        return [
            'prefix' => config('web-pages.api.route_prefix'),
            'middleware' => config('web-pages.api.middleware'),
        ];
    }
}
